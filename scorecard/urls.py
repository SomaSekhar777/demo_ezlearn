from django.urls import path
from . import views

urlpatterns = [
    path('display_scorecard/', views.display_scorecard, name='display_scorecard'),
    path('score_changing/', views.score_changing, name='score_changing'),
    path('reset/', views.reset, name='reset_scorecard')
]