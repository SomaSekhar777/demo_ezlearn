from django.shortcuts import render, redirect
from scorecard.models import Teams

def ball_list():
    l1 = []
    for i in range(0,20):
        for j in range(0,6, 1):
            l1.append(str(i)+'.'+str(j))
    l1.append('20.0')
    return l1

def value_generator(num):
    l1 = ball_list()
    num_ = num
    for index in range(len(l1)):
        if l1[index] == num_:
            num_ = l1[index + 1]
            break
    return num_

ids = []

somu = True
def display_scorecard(request):
    teams = Teams.objects.all()
    for team in teams:
        ids.append(team.id)
    return render(request, 'scorecard/index.html', {'teams': teams})


def score_changing(request):
    team1 = Teams.objects.get(id = ids[0])
    if team1.total_wickets < 10 and team1.total_overs != '20.0' :
        if request.POST.get('4') == '4':
            team1.total_runs += 4
            team1.total_overs = value_generator(team1.total_overs)
        elif request.POST.get('w') == 'WICKET':
            team1.total_wickets += 1
            team1.total_overs = value_generator(team1.total_overs)
        elif request.POST.get('6') == '6':
            team1.total_runs += 6
            team1.total_overs = value_generator(team1.total_overs)
        elif request.POST.get('1') == '1':
            team1.total_runs += 1
            team1.total_overs = value_generator(team1.total_overs)
        elif request.POST.get('2') == '2':
            team1.total_runs += 2
            team1.total_overs = value_generator(team1.total_overs)
        elif request.POST.get('3') == '3':
            team1.total_runs += 3
            team1.total_overs = value_generator(team1.total_overs)
        elif request.POST.get('wd') == 'wd' or request.POST.get('nb') == 'nb' :
            team1.total_runs += 1
        team1.save()
    else:
        team2 = Teams.objects.get(id = ids[1])
        if request.POST.get('4') == '4':
            team2.total_runs += 4
            team2.total_overs = value_generator(team2.total_overs)
        elif request.POST.get('w') == 'WICKET':
            team2.total_wickets += 1
            team2.total_overs = value_generator(team2.total_overs)
        elif request.POST.get('6') == '6':
            team2.total_runs += 6
            team2.total_overs = value_generator(team2.total_overs)
        elif request.POST.get('1') == '1':
            team2.total_runs += 1
            team2.total_overs = value_generator(team2.total_overs)
        elif request.POST.get('2') == '2':
            team2.total_runs += 2
            team2.total_overs = value_generator(team2.total_overs)
        elif request.POST.get('3') == '3':
            team2.total_runs += 3
            team2.total_overs = value_generator(team2.total_overs)
        elif request.POST.get('wd') == 'wd' or request.POST.get('nb') == 'nb' :
            team2.total_runs += 1
        team2.save()
    return redirect('display_scorecard')


def reset(request):
    team1 = Teams.objects.get(id = ids[0])
    team2 = Teams.objects.get(id = ids[1])
    team1.total_runs = 0
    team1.total_wickets = 0
    team1.total_overs = '0.0'
    team1.save()
    team2.total_runs = 0
    team2.total_wickets = 0
    team2.total_overs = '0.0' 
    team2.save()
    return redirect('display_scorecard')






