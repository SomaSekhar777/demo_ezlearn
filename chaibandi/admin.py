from django.contrib import admin
from .models import stock, total_earnings
from django.contrib.auth.models import Permission

admin.site.register([stock, total_earnings])