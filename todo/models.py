from django.db import models

# Create your models here.
class Todo(models.Model):
    task = models.CharField(max_length = 100)
    priority = models.CharField(max_length = 100)
    due_date = models.DateField()

    def __str__(self):
        return f'{self.task} - {self.priority} - {self.due_date}'