from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Student(models.Model):
    name = models.CharField(max_length = 100)
    roll_no = models.CharField(max_length = 10)
    course = models.CharField(max_length = 30)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.name} - {self.roll_no} - {self.course}'

class StudentMark(models.Model):
    subject = models.CharField(max_length = 100)
    score = models.IntegerField()
    student = models.ForeignKey(Student, on_delete=models.CASCADE)

    def __str__(self):
        return f'({self.student.roll_no}) - {self.subject} - {self.score}'
    
class exampleTable(models.Model):
    user=models.OneToOneField(User,on_delete=models.CASCADE)
    example_name = models.CharField(max_length = 100)

class somasekhar(models.Model):
    profile_picture = models.ImageField(null=True,blank=True, upload_to='images/', default='static/dummy.png')

    def __str__(self):
        return f'{self.profile_picture}'

class emailfield(models.Model):
    email = models.EmailField(default='none@gmail.com')

    def __str__(self):
        return f'{self.email}'
    

class course(models.Model):
    course_name = models.CharField(max_length=10)
    activation = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.course_name}'

class learner(models.Model):
    courses = models.ManyToManyField(course)
    student_name = models.CharField(max_length=20)

    def __str__(self):
        return f'{self.student_name}'
    

class learners(models.Model):
        username = models.CharField(max_length=10)
        first_name = models.CharField(max_length=50, default='')
        last_name = models.CharField(max_length=50, default='')
        profile_picture = models.ImageField(null=True, blank=True, upload_to='images/')
        email = models.EmailField()
        ph_number = models.CharField(max_length=15, blank=True, null=True)
        DOB = models.DateField(default='2001-01-01')
        user = models.ForeignKey(User, on_delete=models.CASCADE)

        def __str__(self):
                return f'{self.first_name}'


class developer(models.Model):
        developer_name = models.CharField(max_length=10)
        email = models.EmailField()

        def __str__(self):
                return f'{self.developer_name}'
        
class Course_available(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    price = models.IntegerField()
    discount = models.IntegerField( null=True, blank=True)
    def __str__(self):
         return f'{self.name}'


class learnings(models.Model):
     student = models.ForeignKey(Student, on_delete=models.CASCADE, default=None)
     course_learning = models.OneToOneField(Course_available, on_delete=models.CASCADE, default=None)
     activation = models.BooleanField(default=False)

     def __str__(self):
          return f'{self.student}'
     
     


         
     

        
    
