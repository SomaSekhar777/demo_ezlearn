from django.urls import path
from . import views
urlpatterns = [
    path('all', views.student_list, name='student_list'),
    path('add', views.add_student, name='add_student'),
    path('detail/<int:id>', views.student_detail, name='student_detail'),
    path('delete/<int:id>', views.delete_student, name='delete_student'),
    path('edit/<int:id>', views.update_student, name='update_student'),
    path('detail/<int:id>/add_mark', views.add_mark, name='add_mark'),
    path('detail/<int:id>/edit_mark/<int:mark_id>', views.edit_mark, name='edit_mark'),
    path('login', views.login_view, name='login'),
    path('logout', views.logout_view, name='logout'),
    path('student_register', views.student_register, name='student_register'),
    path('teacher_register', views.teacher_register, name='teacher_register'),
    path('say_hello', views.say_hello, name='say_hello'),
    path('image', views.image_fielf, name='image' ),
    path('showimg', views.show_image, name='show_image')

]